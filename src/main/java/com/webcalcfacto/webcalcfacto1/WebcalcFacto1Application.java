package com.webcalcfacto.webcalcfacto1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebcalcFacto1Application {

	public static void main(String[] args) {
		SpringApplication.run(WebcalcFacto1Application.class, args);
	}
	 
}
