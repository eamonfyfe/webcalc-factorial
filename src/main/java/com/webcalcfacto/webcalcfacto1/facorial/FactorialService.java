package com.webcalcfacto.webcalcfacto1.facorial;

import org.springframework.stereotype.Service;

@Service
public class FactorialService {

	
public int getFactorial(int n) {
	int i = 1;
    int result = 1;
    while (i <= n) {
        result = result * i;
        i++;
    }
    return result;
}
}
