package com.webcalcfacto.webcalcfacto1.facorial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FactorialController {

	@Autowired
	private static FactorialService factorialService;


	@RequestMapping("/factorial")
	public static int factorial(int n) {
		return factorialService.getFactorial(n);

	}
}

